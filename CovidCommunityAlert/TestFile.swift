//
//  TestFile.swift
//  CovidCommunityAlert
//
//  Created by Rodrigo Bueno Tomiosso on 22/04/20.
//  Copyright © 2020 mourodrigo. All rights reserved.
//

import Foundation

public final class TestFile {

    let name = "SwiftyLib"
    
    public func add(a: Int, b: Int) -> Int {
        return a + b
    }
    
    public func sub(a: Int, b: Int) -> Int {
        return a - b
    }
    
}
